@extends('admin.layouts.app')
@section('content')
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-xl-3">
          <div class="card p-2">
            <div class="d-flex justify-content-center mt-4">
              <img class="rounded-circle avatar-xl"
                src="
                @if ($adminData->image) {{ asset('storage/' . $adminData->image) }}
                @else
                https://ui-avatars.com/api/?name=@php echo($adminData->name) @endphp&amp;background=4e73df&amp;color=ffffff&amp;size=100 @endif
                ">
            </div>
            <div class="card-body">
              <h4 class="card-title">
                Name: {{ $adminData->name }}
                <hr />
                User Email: {{ $adminData->email }}
                <hr />
                User Name: {{ $adminData->username }}
                <hr />
              </h4>
              {{-- button edit profile --}}
              <a href="{{ route('admin.edit.profile') }}"
                class="btn btn-info btn-md btn-rounded waves-effect waves-light">
                Edit Profile
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
