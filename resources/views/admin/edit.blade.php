@extends('admin.layouts.app')
@section('content')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Edit Profile Page</h4>
              {{-- form update profile --}}
              <form action="{{ route('admin.update.profile') }}" method="POST" enctype="multipart/form-data">
                @csrf

                {{-- name --}}
                <div class="row mb-3">
                  <label for="name" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                      id="name" value="{{ old('name', $adminData->name) }}">
                    @error('name')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>

                </div>
                {{-- email address --}}
                <div class="row mb-3">
                  <label for="email" class="col-sm-2 col-form-label">Email Address</label>
                  <div class="col-sm-10">
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                      id="email" value="{{ old('email', $adminData->email) }}">
                    @error('email')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>
                </div>
                {{-- username --}}
                <div class="row mb-3">
                  <label for="username" class="col-sm-2 col-form-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" name="username" class="form-control @error('username') is-invalid @enderror"
                      id="username" value="{{ old('username', $adminData->username) }}">
                    @error('username')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>

                </div>
                {{-- image --}}
                <div class="row mb-3">
                  <label for="image" class="col-sm-2 col-form-label">Image</label>
                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror"
                      id="image" />
                    @error('image')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>

                </div>
                {{-- preview image --}}
              
                <div class="row mb-3">
                  <label for="preview" class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-10">
                    <img class="rounded avatar-xl" id="showImage"
                    src="
                    @if ($adminData->image) {{ asset('storage/' . $adminData->image) }}
                    @else
                    https://ui-avatars.com/api/?name=@php echo($adminData->name) @endphp&amp;background=4e73df&amp;color=ffffff&amp;size=100 @endif
                    ">
                  </div>
                </div>
                {{-- button submit --}}
                <button type="submit" class="btn btn-info waves-effect waves-light">Update Profile</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- script for image preview --}}
  <script>
    $(document).ready(function() {
      $('#image').change(function(e) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#showImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(e.target.files['0']);
      });
    });
  </script>
@endsection
