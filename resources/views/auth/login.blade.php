@extends('auth.layouts.app', ['title' => 'Login | Admin', 'title_header' => 'Login'])
@section('content')
  {{-- form login --}}
  <form class="form-horizontal mt-3" method="POST" action="{{ route('login') }}">
    @csrf

    {{-- username --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username"
          value="{{ old('username') }}" placeholder="Username" autofocus autocomplete="username">
        @error('username')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- password --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password"
          placeholder="Password" autocomplete="current-password" />
        @error('password')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- remember me --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" name="remember" class="custom-control-input" id="remember_me">
          <label class="form-label ms-1" for="remember_me">Remember me</label>
        </div>
      </div>
    </div>

    {{-- button submit login --}}
    <div class="form-group mb-3 text-center row mt-3 pt-1">
      <div class="col-12">
        <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Log In</button>
      </div>
    </div>

    <div class="form-group mb-0 row mt-2">
      {{-- button to forgot password --}}
      <div class="col-sm-7 mt-3">
        <a href="{{ route('password.request') }}" class="text-muted">
          <i class="mdi mdi-lock"></i>
          Forgot your password?
        </a>
      </div>
      {{-- button to register --}}
      <div class="col-sm-5 mt-3">
        <a href="{{ route('register') }}" class="text-muted">
          <i class="mdi mdi-account-circle"></i>
          Create an account
        </a>
      </div>
    </div>

  </form>
@endsection
