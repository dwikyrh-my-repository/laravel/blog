@extends('auth.layouts.app', ['title' => 'Register | Admin', 'title_header' => 'Register'])
@section('content')
  {{-- form register user --}}
  <form class="form-horizontal mt-3" method="POST" action="{{ route('register') }}">
    @csrf
    {{-- name --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" name="name"
          value="{{ old('name') }}" placeholder="Name" autofocus autocomplete="name" />
          @error('name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- username --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="username" class="form-control @error('username') is-invalid @enderror" type="text" name="username"
          value="{{ old('username') }}" placeholder="Username" autocomplete="username" />
        @error('username')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- email --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" name="email"
          value="{{ old('email') }}" placeholder="Email Address" autocomplete="email" />
        @error('email')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- password --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password"
          placeholder="Password" autocomplete="new-password" />
        @error('password')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- password confirmation --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="password_confirmation" class="form-control" type="password" name="password_confirmation"
          placeholder="Password Confirmation" autocomplete="new-password" />
      </div>
    </div>

    <div class="form-group mb-3 row">
      <div class="col-12">
        <div class="custom-control custom-checkbox">

        </div>
      </div>
    </div>

    {{-- button submit register --}}
    <div class="form-group text-center row mt-3 pt-1">
      <div class="col-12">
        <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Register</button>
      </div>
    </div>

    {{-- button to login --}}
    <div class="form-group mt-2 mb-0 row">
      <div class="col-12 mt-3 text-center">
        <a href="{{ route('login') }}" class="text-muted">Already have account?</a>
      </div>
    </div>
  </form>
@endsection
