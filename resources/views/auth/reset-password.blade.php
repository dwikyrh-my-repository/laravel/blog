@extends('auth.layouts.app', ['title' => 'Reset Password | Admin',  'title_header' => 'Reset Password'])
@section('content')
  <form method="POST" action="{{ route('password.store') }}">
    @csrf

    {{-- password reset token --}}
    <input type="hidden" name="token" value="{{ $request->route('token') }}">

    {{-- email address --}}
    <div class="form-group mb-3 row">
     <div class="col-12">
        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
        value="{{ old('email', $request->email) }}" placeholder="Email Address" autofocus autocomplete="username" />
      @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
     </div>
    </div>

    {{-- password --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password"
          placeholder="New Password" autocomplete="new-password" />
        @error('password')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    {{-- password confirmation --}}
    <div class="form-group mb-3 row">
      <div class="col-12">
        <input id="password_confirmation" class="form-control" type="password" name="password_confirmation"
          placeholder="Password Confirmation" autocomplete="new-password" />
      </div>
    </div>

    {{-- button submit reset password --}}
    <div class="form-group text-center row mt-3 pt-1">
        <div class="col-12">
          <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Reset Password</button>
        </div>
      </div>
  </form>
@endsection
