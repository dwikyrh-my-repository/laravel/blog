<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- App favicon -->
  <link rel="shortcut icon" href="{{ asset('backend/assets/images/favicon.ico') }}">
  <!-- Bootstrap Css -->
  <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" />
  <!-- Icons Css -->
  <link href="{{ asset('backend/assets/css/icons.min.css') }}" rel="stylesheet" />
  <!-- App Css-->
  <link href="{{ asset('backend/assets/css/app.min.css') }}" id="app-style" rel="stylesheet" />
  {{-- toasttr --}}
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" rel="stylesheet" />
  <title>{{ $title ?? 'Admin' }}</title>
</head>

<body>
  {{-- <div class="bg-overlay"></div> --}}
  <div class="m-4 m-sm-0">
    <div class="wrapper-page">
      <div class="container-fluid p-0">
        <div class="card">
          <div class="card-body">
            <h4 class="text-muted text-center font-size-18 mt-4"><b>{{ $title_header }}</b></h4>
            @if(session('status'))
             <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session('status') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
             </div>
            @endif
            <div class="p-3">
              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- JAVASCRIPT -->
  <script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('backend/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('backend/assets/libs/metismenu/metisMenu.min.js') }}"></script>
  <script src="{{ asset('backend/assets/libs/simplebar/simplebar.min.js') }}"></script>
  <script src="{{ asset('backend/assets/libs/node-waves/waves.min.js') }}"></script>
  <script src="{{ asset('backend/assets/js/app.js') }}"></script>

  {{-- toastr js --}}
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script>
   @if(Session::has('message'))
   var type = "{{ Session::get('alert-type','info') }}"
   switch(type){
      case 'info':
      toastr.info(" {{ Session::get('message') }} ");
      break;
  
      case 'success':
      toastr.success(" {{ Session::get('message') }} ");
      break;
  
      case 'warning':
      toastr.warning(" {{ Session::get('message') }} ");
      break;
  
      case 'error':
      toastr.error(" {{ Session::get('message') }} ");
      break; 
   }
   @endif 
  </script>
</body>

</html>
