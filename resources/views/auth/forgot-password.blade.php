@extends('auth.layouts.app', ['title' => 'Forgot Password | Admin', 'title_header' => 'Forgot Password'])
@section('content')
  {{-- form send link reset password --}}
  <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
    @csrf
    {{-- alert --}}
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      Forgot your password? No problem. Just let us know your email address and we will email you a password
      reset link that will allow you to choose a new one.
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

    {{-- email address --}}
    <div class="form-group mb-3">
      <div class="col-xs-12">
        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
          value="{{ old('email') }}" placeholder="Email Address" />
        @error('email')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>

    <div class="col-sm-7 mt-3">
      <a href="{{ route('login') }}" class="text-muted">
        <i class="mdi mdi-arrow-left"></i>
        Back to login
      </a>
    </div>
    {{-- button send email link reset submit --}}
    <div class="form-group pb-2 text-center row mt-3">
      <div class="col-12">
        <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Send Email</button>
      </div>
    </div>
  </form>
@endsection
