<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# admin all route
Route::controller(AdminController::class)->group(function() {
    # dashboard
    Route::get('/admin/dashboard', 'adminDashboard')->name('admin.dashboard');
    # logout
    Route::get('/admin/logout', 'destroy')->name('admin.logout');
    # show profile
    Route::get('/admin/profile', 'profile')->name('admin.profile');
    # edit profile
    Route::get('/admin/edit/profile', 'editProfile')->name('admin.edit.profile');
    Route::post('/admin/update/profile', 'storeProfile')->name('admin.update.profile');
})->middleware(['auth']);

# brezee profile
Route::middleware('auth', 'verified')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
