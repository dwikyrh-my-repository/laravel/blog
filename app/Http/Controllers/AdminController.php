<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    # admin dashboard
    public function adminDashboard()
    {
        $user = Auth::user()?->id;
        $adminData = User::findOrFail($user);
        return view('admin.index', compact('adminData'));
    }

    # action to logout
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

         # notification
         $notification = [
            'message' => 'User Logout Successfully',
            'alert-type' => 'success',
        ];
        return redirect('/login')->with($notification);
    }

    # profile admin
    public function profile()
    {
        $user = Auth::user()?->id;
        $adminData = User::findOrFail($user);

        return  view('admin.profile', compact('adminData'));
    }

    # edit admin profile
    public function editProfile()
    {
        $user = Auth::user()?->id;
        $adminData = User::findOrFail($user);
        return view('admin.edit', compact('adminData'));
    }

    # update admin profile
    public function storeProfile(Request $request)
    {
        $user = Auth::user()?->id;
        $data = User::findOrFail($user);

        # validation
        $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'username' => ['required', 'alpha_num', 'min:3', 'max:255', Rule::unique('users', 'username')->ignore($user)],
            'email' => ['required', 'email', 'min:3', 'max:255', Rule::unique('users', 'email')->ignore($user)],
            'image' => ['nullable', 'image', 'mimes:png,jpg,jpeg', 'max:2048'],
        ]);

        # get data from form
        $data->name = $request->name;
        $data->email = $request->email;
        $data->username = $request->username;

        # handle image
        $fileRequest = $request->file('image');
        if ($request->hasFile('image')) {
            $filename = date('YmdHi').$fileRequest->getClientOriginalName();
            if ($data['image']) {
                Storage::disk('public')->delete($data['image']);
                $data['image'] = $fileRequest->storeAs('admin/images', $filename);
            } 
            $data['image'] = $fileRequest->storeAs('admin/images', $filename);
         } else {
            $data['image'] = $data->image;
        }
        # save to db
        $data->save();

        # notification
        $notification = [
            'message' => 'Admin Profile Updated Successfully',
            'alert-type' => 'success',
        ];

        # redirect
        return to_route('admin.profile')->with($notification);
    }
}
